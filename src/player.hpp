#ifndef PLAYER_HPP
#define PLAYER_HPP

#include<string>

#include"weapon.hpp"
#include"define.hpp"

class Player : public sf::Drawable, public sf::Transformable {
    
public:

    // Constructor
    Player(int side, int id);
    bool loadFromFile(std::string name);

    // Refresh
    void refresh(float dt);

    // Setters
    void setPosition(int x, int y);
    void dealDamage(int dmg);
    void resetTurn();
    void setShot();
    void setMoved();

    // Getters
    int getX();
    int getY();
    int getHealth();
    bool isDead();
    int getSpeed();
    std::string getName();
    Weapon* getPrimary();
    Weapon* getSecondary();
    int getSide();
    bool canMove();
    bool canShoot();
    bool turnOver();
    bool canDoSomething();

private:

    // Class variables
    int x;
    int y;
    int hp;
    int speed;
    std::string name;
    Weapon primary;
    Weapon secondary;
    int side;
    bool move;
    bool shoot;

    // Deal with damage popup
    float time;
    int recentDmg;

    // Drawing variables
    sf::Texture player_texture;
    sf::Sprite player_sprite;
    sf::Texture damage_texture;
    sf::Sprite damage_sprite;
    sf::Font font;
    sf::Text damage_text;

    // Draw the player
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif
