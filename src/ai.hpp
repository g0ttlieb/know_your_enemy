#ifndef AI_HPP
#define AI_HPP

#include <vector>
#include"player.hpp"
#include"define.hpp"
#include"ui.hpp"

class AI{

public:

    // Constructors
    AI(UI* ui, RangeIndicator* rangeIndicator, Map* map);
    void addPlayers(std::vector<Player*> players);

    // Timeout
    bool timeOutOver(float dt);

    // Process turn
    void performTurn(Player* player);

    // Helper functions
    int getRange(int x, int y, Player* target);
    int getDist(int x, int y, Player* target);
    int getBestWeapon(int dist, Player* player, bool onGrass);

private:

    // Class variables
    float timeout;
    UI* ui;
    Map* map;
    RangeIndicator* rangeIndicator;
    std::vector<Player*> players;

};

#endif
