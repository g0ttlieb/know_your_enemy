#include"define.hpp"
#include"map.hpp"
#include"player.hpp"
#include"range_indicator.hpp"
#include"shot.hpp"
#include"toolbar.hpp"
#include"ui.hpp"
#include"ai.hpp"

#include<dirent.h>
#include<iostream>
#include<unistd.h>

void setTextString(sf::Text* text, std::string string){
    text->setString(string);
    sf::FloatRect bounds = text->getLocalBounds();
    text->setOrigin(bounds.left+bounds.width/2.0f, bounds.top+bounds.height/2.0f);
}

int main(){

    // Change path
    char result[ PATH_MAX ];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
    std::string path = std::string( result, (count > 0) ? count : 0 );
    while(path.back() != '/')
        path.pop_back();
    chdir(path.c_str());

    // Create window
    sf::RenderWindow window;
    window.create(sf::VideoMode(RES_X, RES_Y), "Know Your Enemy", sf::Style::Fullscreen);
    window.setPosition(sf::Vector2i(0,0));
    window.setFramerateLimit(144);

    // FPS Setup
    sf::Font fpsFont;
    fpsFont.loadFromFile("fonts/fps.ttf");
    sf::Text fps;
    fps.setFont(fpsFont);
    fps.setFillColor(sf::Color::Black);
    fps.setCharacterSize(12);
    fps.setPosition(0,0);

    // Init clock
    sf::Clock clock;

    // Setup menu textures;
    sf::Texture main_texture;
    main_texture.loadFromFile("textures/main.png");
    sf::Sprite main_sprite(main_texture);
    sf::Texture about1_texture;
    about1_texture.loadFromFile("textures/about1.png");
    sf::Sprite about1_sprite(about1_texture);
    sf::Texture about2_texture;
    about2_texture.loadFromFile("textures/about2.png");
    sf::Sprite about2_sprite(about2_texture);
    sf::Texture lobby_texture;
    lobby_texture.loadFromFile("textures/lobby.png");
    sf::Sprite lobby_sprite(lobby_texture);
    sf::Texture pause_texture;
    pause_texture.loadFromFile("textures/paused.png");
    sf::Sprite pause_sprite(pause_texture);
    sf::Texture redVict;
    redVict.loadFromFile("textures/redVict.png");
    sf::Texture blueVict;
    blueVict.loadFromFile("textures/blueVict.png");
    sf::Sprite victory;

    // Get available loadouts
    std::vector<std::string> loadouts;
    DIR* dir;
    struct dirent* directory;
    dir = opendir("loadouts/");
    while((directory = readdir(dir)) != NULL ){
        std::string fileName = directory->d_name;
        if(fileName.compare("base") != 0 && fileName[0] != '.')
            loadouts.push_back(fileName);
    }
    loadouts.push_back("?");
    closedir(dir);

    // Setup text for lobby
    sf::Font pixelFont;
    pixelFont.loadFromFile("fonts/pixel.ttf");
    std::vector<sf::Text*> playerText;
    for(int i = RED_START; i < BLUE_END; i++)
        playerText.push_back(new sf::Text("", pixelFont, FONT_SIZE));
    for(unsigned int i = 0; i < playerText.size(); i++)
        playerText[i]->setFillColor(sf::Color::Black);
    playerText[0]->setPosition(PLAYER_X1, PLAYER_Y1);
    playerText[1]->setPosition(PLAYER_X2, PLAYER_Y1);
    playerText[2]->setPosition(PLAYER_X3, PLAYER_Y1);
    playerText[3]->setPosition(PLAYER_X1, PLAYER_Y2);
    playerText[4]->setPosition(PLAYER_X2, PLAYER_Y2);
    playerText[5]->setPosition(PLAYER_X3, PLAYER_Y2);
    sf::Text redAIText("", pixelFont, FONT_SIZE);
    sf::Text blueAIText("", pixelFont, FONT_SIZE);
    redAIText.setPosition(AI_X, AI_Y1);
    blueAIText.setPosition(AI_X, AI_Y2);
    redAIText.setFillColor(sf::Color::Black);
    blueAIText.setFillColor(sf::Color::Black);

    // Build champ select
    sf::Color backgroundColour(164, 145, 121);
    int csRows = loadouts.size()/3;
    if(loadouts.size()%3 != 0) csRows++;
    sf::RectangleShape champSelectBase(sf::Vector2f(3*CS_COL_SIZE, csRows*CS_ROW_SIZE));
    int baseCS_X = 960 - (3*CS_COL_SIZE/2);
    int baseCS_Y = 540 - (csRows*CS_ROW_SIZE/2);
    champSelectBase.setPosition(baseCS_X, baseCS_Y);
    champSelectBase.setFillColor(backgroundColour);
    champSelectBase.setOutlineThickness(4);
    champSelectBase.setOutlineColor(sf::Color::Black);
    sf::FloatRect basePos = champSelectBase.getGlobalBounds();
    std::vector<sf::Text> csTexts;
    baseCS_X += CS_COL_SIZE/2;
    baseCS_Y += CS_ROW_SIZE/2;
    for(int i = 0; i < loadouts.size(); i++){
        sf::Text temp("", pixelFont, FONT_SIZE);
        setTextString(&temp, loadouts[i]);
        temp.setPosition((i%3)*CS_COL_SIZE + baseCS_X, (i/3)*CS_ROW_SIZE + baseCS_Y);
        temp.setFillColor(sf::Color::Black);
        csTexts.push_back(temp);
    }

    // Init all classes
    Map map;
    Toolbar toolbar;
    Shot shot(&map);
    RangeIndicator rangeIndicator(&map);
    std::vector<Player*> players;
    for(int i = 0; i < BLUE_END; i++){
        int side = BLUE;
        if(i < RED_END) side = RED;
        Player* newPlayer = new Player(side, i%3);
        players.push_back(newPlayer);
    }
    UI ui(&map, &rangeIndicator, &shot, &toolbar, loadouts);
    AI ai(&ui, &rangeIndicator, &map);

    // Add players as required
    map.addPlayers(players);
    ui.addPlayers(players);
    ai.addPlayers(players);

    // UI vars
    int activeSide;
    bool shotWasActive;
    bool reloadLobby = true;
    bool quit = false;

    // Main loop
    while(window.isOpen()){

        // Elapsed time
        sf::Time elapsed = clock.restart();
        float dt = elapsed.asSeconds();

        // Process events
        sf::Event e;
        while(window.pollEvent(e)){
            // Quit
            if(e.type == sf::Event::Closed)
                quit = true;
            // Prevent ui acccess when AI's turn
            if(ui.isAI(activeSide) && ui.getMode() >= SELECT){
                if(e.type==sf::Event::KeyPressed &&
                e.key.code==sf::Keyboard::Escape)
                    ui.AIPause();
                continue;
            }
            // CLICK!
            if(e.type == sf::Event::MouseButtonPressed){
                int x = e.mouseButton.x;
                int y = e.mouseButton.y;
                reloadLobby = ui.processClick(x, y, e.mouseButton.button);
            }
            // Keyboard shortcuts
            if(e.type == sf::Event::KeyPressed){
                // Pause / cancel
                if(e.key.code == sf::Keyboard::Escape)
                    ui.escKey();
                // Skip
                if(e.key.code == sf::Keyboard::Space)
                    ui.processClick(1900, 1050, LCLICK);
                // Move
                if(e.key.code == sf::Keyboard::Q)
                    ui.processClick(1900, 150, LCLICK);
                // Shoot primary
                if(e.key.code == sf::Keyboard::W)
                    ui.processClick(1900, 300, LCLICK);
                // Shoot secondary
                if(e.key.code == sf::Keyboard::E)
                    ui.processClick(1900, 700, LCLICK);
                // Next player
                if(e.key.code == sf::Keyboard::R)
                    ui.selectNextPlayer();
            }
        }

        // Quit
        if(quit || ui.getMode() == QUIT)
            break;

        // Clear the floor!
        window.clear();

        // Display menus
        int mode = ui.getMode();
        if(mode < SELECT){
            if(mode == MAIN){
                window.draw(main_sprite);
                window.display();
                continue;
            }
            else if(mode == ABOUT1){
                window.draw(about1_sprite);
                window.display();
                continue;
            }
            else if(mode == ABOUT2){
                window.draw(about2_sprite);
                window.display();
                continue;
            }
            else if(mode == LOBBY){
                window.draw(lobby_sprite);
                if(reloadLobby){
                    for(unsigned int i = 0; i < players.size(); i++)
                        setTextString(playerText[i], players[i]->getName());
                    if(ui.isAI(RED))
                        setTextString(&redAIText, "AI");
                    else
                        setTextString(&redAIText, "Human");
                    if(ui.isAI(BLUE))
                        setTextString(&blueAIText, "AI");
                    else
                        setTextString(&blueAIText, "Human");
                }
                for(unsigned int i = 0; i < playerText.size(); i++)
                    window.draw(*playerText[i]);
                window.draw(redAIText);
                window.draw(blueAIText);

                window.display();
                activeSide = BLUE;
                shotWasActive = false;
                continue;
            }
            else if(mode == CHAMP_SELECT){
                window.draw(lobby_sprite);
                for(unsigned int i = 0; i < playerText.size(); i++)
                    window.draw(*playerText[i]);
                window.draw(redAIText);
                window.draw(blueAIText);
                window.draw(champSelectBase);
                for(int i = 0; i < csTexts.size(); i++)
                    window.draw(csTexts[i]);
                window.display();
                continue;
            }
            else if(mode == PAUSE || mode == VICTORY){
                window.draw(map);
                window.draw(rangeIndicator);
                for(unsigned int i = 0; i < players.size(); i++)
                    window.draw(*players[i]);
                window.draw(shot);
                window.draw(toolbar);
                if(mode == PAUSE)
                    window.draw(pause_sprite);
                else
                    window.draw(victory);
                window.display();
                continue;
            }
        }
        // Else process game loop

        // Victory cond
        if(players[0]->isDead() && players[1]->isDead() && players[2]->isDead()){
            ui.setVict();
            victory.setTexture(blueVict);
        }
        else if(players[3]->isDead() && players[4]->isDead() && players[5]->isDead()){
            ui.setVict();
            victory.setTexture(redVict);
        }

        // Turn reset
        if(activeSide == RED && !players[0]->canDoSomething() && !players[1]->canDoSomething() && !players[2]->canDoSomething()){
            activeSide = BLUE;
            ui.setSide(activeSide);
            toolbar.setSide(activeSide);
            // Set new active player
            for(int i = BLUE_START; i < BLUE_END; i++){
                Player* current = players[i];
                if(!current->isDead()){
                    ui.setActivePlayer(current);
                    break;
                }
            }
            // Reset turn
            for(int i = BLUE_START; i < BLUE_END; i++)
                players[i]->resetTurn();
        }
        else if(activeSide == BLUE && !players[3]->canDoSomething() && !players[4]->canDoSomething() && !players[5]->canDoSomething()){
            activeSide = RED;
            ui.setSide(activeSide);
            toolbar.setSide(activeSide);
            // Set new active player
            for(int i = RED_START; i < RED_END; i++){
                Player* current = players[i];
                if(!current->isDead()){
                    ui.setActivePlayer(current);
                    break;
                }
            }
            // Reset turn
            for(int i = RED_START; i < RED_END; i++)
                players[i]->resetTurn();

        }

        // Refresh range indicator if not during or directly after shot
        if(shot.isActive())
            shotWasActive = true;
        else if(shotWasActive){
            shotWasActive = false;
            rangeIndicator.showNone();
            ui.checkStillAlive();
            toolbar.refresh();
        }
        else{
            sf::Vector2i mousePos = sf::Mouse::getPosition();
            rangeIndicator.refresh(mousePos.x, mousePos.y);
        }

        // AI
        if(ui.isAI(activeSide) && !shot.isActive() && ai.timeOutOver(dt)){
            if(activeSide == RED){
                for(unsigned int i = RED_START; i < RED_END; i++)
                    ai.performTurn(players[i]);
            }
            else{
                for(unsigned int i = BLUE_START; i < BLUE_END; i++)
                    ai.performTurn(players[i]);
            }
        }

        // Refresh others
        shot.refresh(dt);
        for(unsigned int i = 0; i < players.size(); i++)
            players[i]->refresh(dt);

        // Draw everything
        window.draw(map);
        window.draw(rangeIndicator);
        for(unsigned int i = 0; i < players.size(); i++)
            window.draw(*players[i]);
        window.draw(shot);
        window.draw(toolbar);

        // Draw fps
        fps.setString("FPS: " + std::to_string(1/elapsed.asSeconds()));
        window.draw(fps);

        // Display
        window.display();

    }

    // Free allocated memory
    rangeIndicator.freeMem();
    for(int i = 0; i < players.size(); i++)
        delete players[i];
    for(int i = 0; i < playerText.size(); i++)
        delete playerText[i];
}
