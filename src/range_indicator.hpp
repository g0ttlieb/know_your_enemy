#ifndef RANGE_INDICATOR_HPP
#define RANGE_INDICATOR_HPP

#include"player.hpp"
#include"map.hpp"
#include"define.hpp"

class RangeIndicator : public sf::Drawable, public sf::Transformable {
    
public:

    // Constructor
    RangeIndicator(Map* map);

    // Change which grid displayed
    void setPlayer(Player* player);
    void showMove();
    void showPrimary();
    void showSecondary();
    void showNone();
    bool validMove(int x, int y);
    void refresh(int x, int y);
    
    // Delete the allocated memory
    void freeMem();

private:

    // Class variables
    Player* player;
    Map* map;
    bool showArc;
    bool firstTime;
    sf::Texture ri_texture;
    sf::VertexArray ri_vertices;
    sf::ConvexShape rangeArc;

    // Store reachable squares
    int** dist;
    int baseX;
    int baseY;
    int capX;
    int capY;

    // Helper functions
    void addPoint(int index, int x, int y);
    void recurseDist(int x, int y);
    void createArc(Weapon* weapon);

    // Draw the player
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif
