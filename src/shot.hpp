#ifndef SHOT_HPP
#define SHOT_HPP

#include"weapon.hpp"
#include"map.hpp"
#include"define.hpp"

class Shot : public sf::Drawable, public sf::Transformable {

public:

    // Constructor
    Shot(Map* map);

    // Interate with shot
    void startShot(int origX, int origY, Weapon* weapon, int destX, int destY);
    bool isActive();
    void setInactive();
    void refresh(float dt);

private:

    // Class variables
    Map* map;
    Weapon* weapon;
    bool active;

    // Angle info
    float centAngle;
    float sideAngle;
    float actlAngle;
    float maxRange;

    // Blast weapon damage
    std::vector<std::pair<int,int>> damageLocations;
    float timeout;

    // High level info
    int remainingShots;
    int dmgPerShot;
    bool newShot;
    int origX;
    int origY;

    // Pos info
    float prevX;
    float prevY;

    // Grass dodge
    int dodgeX;
    int dodgeY;

    // Drawing variables
    sf::Texture small_texture;
    sf::Texture large_texture;
    sf::Texture damage_texture;
    sf::Sprite shot_sprite;

    // Helper functions
    void startNewShot();
    void dealDamage(int x, int y);
    void addDamageSprite(int x, int y);

    // Draw the player
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif
