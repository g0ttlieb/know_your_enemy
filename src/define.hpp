#ifndef DEFINE_HPP
#define DEFINE_HPP

    // SFML Libs
    #include<SFML/Window.hpp>
    #include<SFML/Graphics.hpp>
    #include<SFML/System.hpp>

    // Window info
    #define RES_X 1920
    #define RES_Y 1080

    // Toolbar formatting
    #define NUM_FONT_SIZE 60
    #define WORD_FONT_SIZE 52
    #define BOOL_FONT_SIZE 44
    #define TOOLBAR_TXT_X_POS 1868

    // Board info
    #define TILE_X 48
    #define TILE_Y 30
    #define BLOCK 36

    // Map starter info
    #define N_ROCKS 52
    #define N_RIVER 144
    #define N_GRASS 52

    // Tile info
    #define GROUND 0
    #define RIVER 1
    #define GRASS 2
    #define ROCK 3
    #define PLAYER 10

    // Side info
    #define RED 0
    #define BLUE 1

    // Shot accuracy info
    #define BASE_ACC_RANGE 1
    #define SCALING_ACC_RANGE 1
    #define BASE_MAX_RANGE 3
    #define SCALING_MAX_RANGE 2
    #define INCREMENT 0.001

    // Shot info
    #define SHOT_SPEED 500
    #define BLAST_DMG_DIVIDE 3
    #define GRASS_DODGE 3
    #define DAMAGE_TIMEOUT 1.5f

    // Spawn locations
    #define SPAWN_X_RED 11
    #define SPAWN_X_BLUE 36
    #define SPAWN_Y_1 7
    #define SPAWN_Y_2 14
    #define SPAWN_Y_3 21

    // Mode info
    #define MAIN 0
    #define ABOUT1 1
    #define ABOUT2 2
    #define LOBBY 3
    #define CHAMP_SELECT 4
    #define PAUSE 5
    #define VICTORY 6
    #define SELECT 7
    #define MOVE 8
    #define SHOT_PRI 9
    #define SHOT_SEC 10
    #define QUIT 11

    // AI
    #define AI_TIMEOUT 1.0f
    #define AI_RED_DEFAULT false
    #define AI_BLUE_DEFAULT true

    // Champ select
    #define PLAYER11_LOADOUT "Assault"
    #define PLAYER12_LOADOUT "Heavy"
    #define PLAYER13_LOADOUT "Fragger"
    #define PLAYER21_LOADOUT "?"
    #define PLAYER22_LOADOUT "?"
    #define PLAYER23_LOADOUT "?"

    // Lobby info
    #define FONT_SIZE 50
    #define PLAYER_Y1 360
    #define PLAYER_Y2 720
    #define PLAYER_X1 710
    #define PLAYER_X2 960
    #define PLAYER_X3 1210
    #define AI_Y1 460
    #define AI_Y2 620
    #define AI_X 960

    // Champ select info
    #define CS_COL_SIZE 150
    #define CS_ROW_SIZE 75

    // AI Click
    #define TOOLBAR_X 1900
    #define TOOLBAR_MOVE 100
    #define TOOLBAR_PRI 300
    #define TOOLBAR_SEC 800
    #define TOOLBAR_SKIP 1050

    // Left / right click
    #define LCLICK sf::Mouse::Button::Left
    #define RCLICK sf::Mouse::Button::Right

    // AI move stuff
    #define L_NUM 1000
    #define S_NUM 5

    // Index
    #define RED_START 0
    #define RED_END 3
    #define BLUE_START 3
    #define BLUE_END 6

#endif
