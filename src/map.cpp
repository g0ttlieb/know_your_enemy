#include"map.hpp"

#include<iostream>

// Constructor
Map::Map(){
    map_texture.loadFromFile("textures/tile.png");
    map_vertices.setPrimitiveType(sf::Quads);
}

// Generate new level and load appropriate sprite
void Map::reroll(){
    int rocks = generateLevel();
    map_vertices.resize(((TILE_X*TILE_Y) + rocks)*4);
    loadSprite();
}

// Load all of the players
void Map::addPlayers(std::vector<Player*> players){
    this->players = players;
}

// Get terrain at pos
int Map::getTerrain(int x, int y){
    int val = level[y][x];
    if(getPlayer(x,y) != NULL) val += PLAYER;
    return val;
}

// Get pointer to actual player at the pos
Player* Map::getPlayer(int x, int y){
    for(unsigned int i = 0; i < players.size(); i++){
        Player* current = players[i];
        if(!current->isDead() && current->getX()==x && current->getY()==y)
            return current;
    }
    return NULL;
}

// Generate level setup: returns rocks
int Map::generateLevel(){

    // Setup seed
    srand(time(NULL));

    // Init to open ground
    for(int i = 0; i < TILE_Y; i++){
        for(int j = 0; j < TILE_X; j++)
            level[i][j] = GROUND;
    }

    // Init variables
    int dir;
    int pos_x;
    int pos_y;
    int placed;

    // Initial river position
    dir = rand()%2;
    pos_y = 0 + ((rand()%2) * TILE_Y);
    pos_x = TILE_X / 4 + rand()%(TILE_X/2);

    // Place river
    placed = 0;
    while(placed < N_RIVER){

        // Place if legit
        if(pos_x < 0)
            pos_x = 0;
        else if(pos_x >= TILE_X)
            pos_x = TILE_X - 1;
        else if(pos_y < 0)
            pos_y = 0;
        else if(pos_y >= TILE_Y)
            pos_y = TILE_Y - 1;
        else if(level[pos_y][pos_x] == GROUND){
            level[pos_y][pos_x] = RIVER;
            placed++;
        }

        // Generate next pos
        dir = rand()%4;
        if(dir == 0)
            pos_y -= 1;
        else if(dir == 1)
            pos_x += 1;
        else if(dir == 2)
            pos_y += 1;
        else
            pos_x -= 1;

    }

    // Place grass
    placed = 0;
    while(placed < N_GRASS){
        pos_x = rand()%TILE_X;
        pos_y = rand()%TILE_Y;
        if(level[pos_y][pos_x] != RIVER){
            level[pos_y][pos_x] = GRASS;
            placed++;
        }
    }

    // Place rocks
    int total_rocks = 0;
    for(int i = 0; i < N_ROCKS; i++){

        // Place origin rock
        pos_x = rand()%TILE_X;
        pos_y = rand()%TILE_Y;
        if(!(level[pos_y][pos_x] / ROCK)){
            level[pos_y][pos_x] += ROCK;
            total_rocks++;
        }

        // RNG Clusters
        while(rand()%5 > 2){
            if(rand()%2)
                pos_x += -1 + 2*(rand()%2);
            else
                pos_y += -1 + 2*(rand()%2);
            pos_x = pos_x % TILE_X;
            pos_y = pos_y % TILE_Y;
            if(!(level[pos_y][pos_x] / ROCK)){
                level[pos_y][pos_x] += ROCK;
                total_rocks++;
            }
        }
    }

    // Make player spawns solid ground
    level[SPAWN_Y_1][SPAWN_X_RED] = GROUND;
    level[SPAWN_Y_2][SPAWN_X_RED] = GROUND;
    level[SPAWN_Y_3][SPAWN_X_RED] = GROUND;
    level[SPAWN_Y_1][SPAWN_X_BLUE] = GROUND;
    level[SPAWN_Y_2][SPAWN_X_BLUE] = GROUND;
    level[SPAWN_Y_3][SPAWN_X_BLUE] = GROUND;

    return total_rocks;

}

// Load sprite for level
void Map::loadSprite(){

    // Iterate over all positions
    int type;
    int pos = 0;
    for(int i = 0; i < TILE_Y; i++){
        for(int j = 0; j < TILE_X; j++){

            // Set pos
            map_vertices[pos].position = sf::Vector2f(j*BLOCK, i*BLOCK);
            map_vertices[pos+1].position = sf::Vector2f((j+1)*BLOCK, i*BLOCK);
            map_vertices[pos+2].position = sf::Vector2f((j+1)*BLOCK, (i+1)*BLOCK);
            map_vertices[pos+3].position = sf::Vector2f(j*BLOCK, (i+1)*BLOCK);

            // Set texture
            type = level[i][j]%ROCK;
            map_vertices[pos].texCoords = sf::Vector2f(type*BLOCK, 0);
            map_vertices[pos+1].texCoords = sf::Vector2f((type+1)*BLOCK, 0);
            map_vertices[pos+2].texCoords = sf::Vector2f((type+1)*BLOCK, BLOCK);
            map_vertices[pos+3].texCoords = sf::Vector2f(type*BLOCK, BLOCK);

            // Update pos
            pos+=4;

            // Add rock if present
            if((level[i][j] / ROCK)){

                // Pos
                map_vertices[pos].position = sf::Vector2f(j*BLOCK, i*BLOCK);
                map_vertices[pos+1].position = sf::Vector2f((j+1)*BLOCK, i*BLOCK);
                map_vertices[pos+2].position = sf::Vector2f((j+1)*BLOCK, (i+1)*BLOCK);
                map_vertices[pos+3].position = sf::Vector2f(j*BLOCK, (i+1)*BLOCK);

                // Texture
                type = ROCK;
                map_vertices[pos].texCoords = sf::Vector2f(type*BLOCK, 0);
                map_vertices[pos+1].texCoords = sf::Vector2f((type+1)*BLOCK, 0);
                map_vertices[pos+2].texCoords = sf::Vector2f((type+1)*BLOCK, BLOCK);
                map_vertices[pos+3].texCoords = sf::Vector2f(type*BLOCK, BLOCK);

                // Update pos
                pos+=4;
            }

        }
    }

}

// Draw the map
void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    states.texture = &map_texture;
    target.draw(map_vertices, states);
}
