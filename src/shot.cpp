#include"shot.hpp"

#include<iostream>
#include<cmath>

// Constructor
Shot::Shot(Map* map){

    // Store map and set inactive
    this->map = map;
    active = false;

    // Init dodge to none
    dodgeX = -1;
    dodgeY = -1;

    // Load up sprites
    small_texture.loadFromFile("textures/bullet.png");
    large_texture.loadFromFile("textures/missile.png");
    damage_texture.loadFromFile("textures/damage.png");
    shot_sprite.setOrigin(12,12);

    // Setup RNGesus
    srand(time(NULL));

}

// Start new shot
void Shot::startShot(int origX, int origY, Weapon* weapon, int destX, int destY){

    // Set appropriate texture and origin
    if(weapon->isBlast())
        shot_sprite.setTexture(large_texture);
    else
        shot_sprite.setTexture(small_texture);

    // Set shot info
    active = true;
    this->weapon = weapon;
    this->origX = origX;
    this->origY = origY;
    newShot = true;

    // Calculate num shots and damage each
    if(weapon->isSingleShot() && !weapon->isBlast()){
        dmgPerShot = weapon->getDamage();
        remainingShots = 1;
    }
    else if(!weapon->isSingleShot() && !weapon->isBlast()){
        dmgPerShot = 1;
        remainingShots = weapon->getDamage();
    }
    else if(weapon->isSingleShot() && weapon->isBlast()){
        dmgPerShot = weapon->getDamage() / BLAST_DMG_DIVIDE;
        remainingShots = 1;
    }
    else if(!weapon->isSingleShot() && weapon->isBlast()){
        dmgPerShot = 1;
        remainingShots = weapon->getDamage() / BLAST_DMG_DIVIDE;
        if(remainingShots == 0){
            remainingShots = 1;
            dmgPerShot = 0;
        }
    }

    // Calc acc and range
    int acc = weapon->getAccuracy();
    if(map->getTerrain(origX, origY) == GRASS + PLAYER)
        acc /= 2;
    maxRange=(BASE_MAX_RANGE+SCALING_MAX_RANGE*acc)*BLOCK;

    // Calc side angle
    float rad_to_deg = 180.0f/(4.0f*std::atan(1));
    float accRange=(BASE_ACC_RANGE+SCALING_ACC_RANGE*acc)*BLOCK;
    sideAngle=std::atan((float)BLOCK/(2.0f*accRange))*rad_to_deg;

    // Get diffs
    int xDiff = destX - origX*BLOCK - 0.5f*BLOCK;
    int yDiff = destY - origY*BLOCK - 0.5f*BLOCK;
    if(xDiff == 0) xDiff = 0.0001f;                     // Prevent / 0

    // Calc center angle
    centAngle = std::atan(((float) yDiff)/((float) xDiff)) * rad_to_deg;
    if(destX >= origX*BLOCK + 0.5f*BLOCK)
        centAngle+=90;
    else
        centAngle+=270;

}

// Yup ...
bool Shot::isActive(){ return active; }
void Shot::setInactive(){ active = false; }

// Continue with shot
void Shot::refresh(float dt){

    // Update timeout
    if(timeout < DAMAGE_TIMEOUT)
        timeout += dt;
    else if(timeout > DAMAGE_TIMEOUT){
        timeout = DAMAGE_TIMEOUT;
        damageLocations.clear();
    }

    // If not active get outta here
    if(!active) return;

    // No remaining shots
    if(remainingShots <= 0){
        active = false;
        newShot = false;
        return;
    }

    // If need new shot
    if(newShot){
        startNewShot();
        newShot = false;
        return;
    }

    // Calc gradients
    float deg_to_rad = (4.0f*std::atan(1))/180;
    float xCoef;
    float yCoef;
    if(actlAngle >= 0 && actlAngle < 90){
        xCoef = std::cos((90-actlAngle) * deg_to_rad);
        yCoef = -std::sin((90-actlAngle) * deg_to_rad);
    }
    else if(actlAngle >= 90 && actlAngle < 180){
        xCoef = std::cos((actlAngle-90) * deg_to_rad);
        yCoef = std::sin((actlAngle-90) * deg_to_rad);
    }
    else if(actlAngle >= 180 && actlAngle < 270){
        xCoef = -std::cos((270-actlAngle) * deg_to_rad);
        yCoef = std::sin((270-actlAngle) * deg_to_rad);
    }
    else{
        xCoef = -std::cos((actlAngle-270) * deg_to_rad);
        yCoef = -std::sin((actlAngle-270) * deg_to_rad);
    }

    // Calc new pos and move
    float newX = prevX + SHOT_SPEED*dt*xCoef;
    float newY = prevY + SHOT_SPEED*dt*yCoef;
    if(newX == prevX) newX += 0.0001f;
    shot_sprite.setPosition(newX, newY);

    // Check collision
    float grad = (newY - prevY) / (newX - prevX);
    int x = prevX;
    int y = prevY;
    int coordX;
    int coordY;
    int terrain;
    int dist;
    while(true){

        // Get terrain
        coordX = x / BLOCK;
        coordY = y / BLOCK;
        terrain = map->getTerrain(coordX, coordY);

        // Calc distance
        int xDist = std::pow(x - (origX*BLOCK) - (BLOCK/2),2);
        int yDist = std::pow(y - (origY*BLOCK) - (BLOCK/2),2);
        dist = std::sqrt(xDist + yDist);

        // Start block
        if(coordX == origX && coordY == origY);

        // Dodge block (due to grass RNG)
        else if(coordX == dodgeX && coordY == dodgeY);

        // No obstacles and range
        else if(terrain < ROCK && dist < maxRange);

        // Impact or out of range
        else if(terrain >= ROCK || dist >= maxRange){

            // Grass miss chance
            if(terrain == GRASS + PLAYER && !(rand()%GRASS_DODGE)){
                dodgeX = coordX;
                dodgeY = coordY;
            }

            // BOOM!
            else{
                timeout = 0;
                // Deal damage
                if(!weapon->isBlast()){
                    addDamageSprite(coordX, coordY);
                    dealDamage(coordX, coordY);
                }
                else{
                    // 1/3 Damage to outside
                    for(int i = -1; i <= 1; i++){
                        for(int j = -1; j <= 1; j++){
                            addDamageSprite(coordX + i, coordY + j);
                            dealDamage(coordX + i, coordY + j);
                        }
                    }
                    // Full damage to center
                    for(int i = 0; i < (weapon->getDamage()/dmgPerShot)-1; i++)
                        dealDamage(coordX, coordY);
                }

                // Book keeping
                remainingShots--;
                newShot = true;
                break;
            }

        }

        // Update checking pos and break conditions
        // Update dist
        if(std::abs(grad) > 1){
            if(prevY < newY){
                y++;
                x+=1/grad;
                if(y > newY)
                    break;
            }
            else{
                y--;
                x-=1/grad;
                if(y < newY)
                    break;
            }
        }
        else{
            if(prevX < newX){
                x++;
                y+=grad;
                if(x > newX)
                    break;
            }
            else{
                x--;
                y-=grad;
                if(x < newX)
                    break;
            }
        }

    }

    // Update prev
    prevX = newX;
    prevY = newY;
}

// Generate the new shot
void Shot::startNewShot(){

    // RNG Angle and set it + pos
    float percent = (rand()%100)/100.0f;
    actlAngle = centAngle - sideAngle + 2*(sideAngle*percent);
    shot_sprite.setRotation(actlAngle);
    prevX = origX*BLOCK + BLOCK/2;
    prevY = origY*BLOCK + BLOCK/2;
    shot_sprite.setPosition(prevX, prevY);

    // Reset dodge
    dodgeX = -1;
    dodgeY = -1;

}


void Shot::addDamageSprite(int x, int y){
    if(map->getPlayer(x,y) == NULL)
        damageLocations.push_back(std::make_pair(x,y));
}

// Deal damamge to player
void Shot::dealDamage(int x, int y){
    if(x < 0 || y < 0 || x >= TILE_X || y >= TILE_Y);
    else if(map->getPlayer(x,y) == NULL);
    else map->getPlayer(x,y)->dealDamage(dmgPerShot);
}

// Draw the sprite if active
void Shot::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    if(active)
        target.draw(shot_sprite);
    for(unsigned int i = 0; i < damageLocations.size(); i++){
        std::pair<int, int> current = damageLocations[i];
        if(map->getPlayer(current.first,current.second) != NULL)
            continue;
        sf::Sprite currentSprite(damage_texture);
        currentSprite.setPosition(BLOCK*current.first, BLOCK*current.second);
        target.draw(currentSprite);
    }
}
