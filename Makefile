# Main target
know_your_enemy: obj/know_your_enemy.o obj/map.o obj/player.o obj/toolbar.o obj/range_indicator.o obj/weapon.o obj/shot.o obj/ui.o obj/ai.o
	g++ obj/*.o -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -o know_your_enemy -std=c++11 -no-pie


# Individual object files
obj/know_your_enemy.o: src/know_your_enemy.cpp
	g++ src/know_your_enemy.cpp -c -std=c++11 -o obj/know_your_enemy.o
	
obj/map.o: src/map.cpp
	g++ src/map.cpp -c -std=c++11 -o obj/map.o

obj/player.o: src/player.cpp
	g++ src/player.cpp -c -std=c++11 -o obj/player.o

obj/toolbar.o: src/toolbar.cpp
	g++ src/toolbar.cpp -c -std=c++11 -o obj/toolbar.o

obj/range_indicator.o: src/range_indicator.cpp
	g++ src/range_indicator.cpp -c -std=c++11 -o obj/range_indicator.o

obj/weapon.o: src/weapon.cpp
	g++ src/weapon.cpp -c -std=c++11 -o obj/weapon.o

obj/shot.o: src/shot.cpp
	g++ src/shot.cpp -c -std=c++11 -o obj/shot.o

obj/ui.o: src/ui.cpp
	g++ src/ui.cpp -c -std=c++11 -o obj/ui.o

obj/ai.o: src/ai.cpp
	g++ src/ai.cpp -c -std=c++11 -o obj/ai.o

# Commands
clean:
	rm obj/*.o know_your_enemy

all:
	rm obj/*.o know_your_enemy
	$(MAKE)
