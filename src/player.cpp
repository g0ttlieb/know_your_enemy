#include"player.hpp"

#include<iostream>
#include<fstream>

// Constructor
Player::Player(int side, int id){

    // Set info
    this->side = side;
    move = false;
    shoot = false;

    // Init vars
    recentDmg = 0;
    time = DAMAGE_TIMEOUT;

    // Load relevant texture into sprite
    if(side == RED){
        if(id == 0)
            player_texture.loadFromFile("textures/player11.png");
        else if(id == 1)
            player_texture.loadFromFile("textures/player12.png");
        else if(id == 2)
            player_texture.loadFromFile("textures/player13.png");
    }
    else{
        if(id == 0)
            player_texture.loadFromFile("textures/player21.png");
        else if(id == 1)
            player_texture.loadFromFile("textures/player22.png");
        else if(id == 2)
            player_texture.loadFromFile("textures/player23.png");
    }
    damage_texture.loadFromFile("textures/damage.png");

    // Load font
    font.loadFromFile("fonts/pixel.ttf");
    damage_text.setFont(font);
    damage_text.setOrigin(BLOCK/2, BLOCK/2);

    // Load texture into sprite
    player_sprite.setTexture(player_texture);
    damage_sprite.setTexture(damage_texture);

}

// Load player specs from file
bool Player::loadFromFile(std::string name){

    if(name.compare("?") == 0){
        this->name = "?";
        return true;
    }

    // Open file if exists
    std::ifstream file("loadouts/" + name);
    if(!file)
        return false;

    // Init
    int dmg;
    int acc;
    int blst;
    int type;

    // Load player info
    file >> this->name;
    file >> hp;
    file >> speed;

    // Load primary weapon
    file >> dmg;
    file >> acc;
    file >> blst;
    file >> type;
    primary = Weapon(dmg, acc, (bool) blst, (bool) type);

    // Load primary weapon
    file >> dmg;
    file >> acc;
    file >> blst;
    file >> type;
    secondary = Weapon(dmg, acc, (bool) blst, (bool) type);

    // Init variables
    time = DAMAGE_TIMEOUT;

    // Close and return success
    file.close();
    return true;

}

// Refresh
void Player::refresh(float dt){

    // Process time diff
    if(time >= DAMAGE_TIMEOUT){
        recentDmg = 0;
        return;
    }
    time += dt;

}

// Set position (relative to grid)
void Player::setPosition(int x, int y){

    // Update pos
    this->x = x;
    this->y = y;

    // Update sprite positions
    player_sprite.setPosition(x*BLOCK, y*BLOCK);
    damage_text.setPosition(x*BLOCK + BLOCK/2, y*BLOCK + BLOCK/2);
    damage_sprite.setPosition(x*BLOCK, y*BLOCK);

}

// Process damage
void Player::dealDamage(int dmg){

    // Deal dmg
    hp -= dmg;

    // Update damage visual
    time = 0;
    recentDmg += dmg;
    if(hp > 0)
        damage_text.setString(std::to_string(recentDmg));
    else
        damage_text.setString("RIP");
    sf::FloatRect pos = damage_text.getLocalBounds();
    damage_text.setOrigin(pos.left+pos.width/2.0f, pos.top+pos.height/2.0f);

}

// Turn setters
void Player::resetTurn(){
    move = true;
    shoot = true;
}
void Player::setShot(){ shoot = false; }
void Player::setMoved(){ move = false; }

// Stat getters
int Player::getX(){ return x; }
int Player::getY(){ return y; }
int Player::getHealth(){ return hp; }
bool Player::isDead(){ return (hp <= 0); }
int Player::getSpeed(){ return speed; }
std::string Player::getName(){ return name; }
Weapon* Player::getPrimary(){ return &primary; }
Weapon* Player::getSecondary(){ return &secondary; }
int Player::getSide(){ return side; }

// Turn getters
bool Player::canMove(){ return move; }
bool Player::canShoot(){ return shoot; }
bool Player::turnOver(){ return !(move || shoot); }
bool Player::canDoSomething(){ return !isDead() && !turnOver(); }

// Draw the sprite
void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const{

    // Dont draw is dead and damage no longer showing
    if(hp <= 0 && time >= DAMAGE_TIMEOUT) return;

    // Otherwises ... you can proceed
    states.transform *= getTransform();
    states.texture = &player_texture;
    target.draw(player_sprite, states);

    // Draw damage over the top if within timeout
    if(time < DAMAGE_TIMEOUT){
        target.draw(damage_sprite);
        target.draw(damage_text);
    }


}
