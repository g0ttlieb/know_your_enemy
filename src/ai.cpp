#include"ai.hpp"

#include<iostream>
#include<cmath>

// Constructors
AI::AI(UI* ui, RangeIndicator* rangeIndicator, Map* map){
    this->ui = ui;
    this->rangeIndicator = rangeIndicator;
    this->map = map;
    timeout = AI_TIMEOUT;
}

// Add players
void AI::addPlayers(std::vector<Player*> players){
    this->players = players;
}

bool AI::timeOutOver(float dt){
    if(timeout > 0){
        timeout -= dt;
        return false;
    }
    return true;
}

// Process given turn
void AI::performTurn(Player* player){

    // If cant do anything or waiting on timeout: skip
    if(!player->canDoSomething() || timeout > 0)
        return;

    // Get opposition
    std::vector<Player*> enemy;
    if(player->getSide() == RED){
        for(int i = BLUE_START; i < BLUE_END; i++)
            enemy.push_back(players[i]);
    }
    else{
        for(int i = RED_START; i < RED_END; i++)
            enemy.push_back(players[i]);
    }

    // Get own position
    int playX = player->getX();
    int playY = player->getY();

    // Compute shot
    if(player->canShoot()){

        // Get target: prioritise KO then damage
        Player* target = NULL;
        int maxDmg = 0;
        int dmg;
        bool KO = false;
        bool onGrass = (map->getTerrain(playX, playY) == GRASS+PLAYER);
        for(unsigned int i = 0; i < enemy.size(); i++){
            Player* current = enemy[i];
            int range = getRange(playX, playY, current);
            if(range > 0 && (dmg=getBestWeapon(range, player, onGrass)) > 0){
                target = current;
                maxDmg = dmg;
                if(target->getHealth() - dmg <= 0) KO = true;
                break;
            }
        }

        // Do shot
        if(target != NULL){

            // Enable shot
            if(maxDmg == player->getPrimary()->getDamage())
                ui->processClick(TOOLBAR_X, TOOLBAR_PRI, LCLICK);
            else
                ui->processClick(TOOLBAR_X, TOOLBAR_SEC, LCLICK);

            // Click!
            int clickX = target->getX()*BLOCK + BLOCK/2;
            int clickY = target->getY()*BLOCK + BLOCK/2;
            rangeIndicator->refresh(clickX, clickY);
            ui->processClick(clickX, clickY, LCLICK);

            // Enable timeout
            timeout = AI_TIMEOUT;
        }
    }

    // If shot then timeout before move
    if(timeout > 0)
        return;
    // Already used move and now skipped shoot so skip
    else if(!player->canMove()){
        ui->processClick(TOOLBAR_X, TOOLBAR_SKIP, LCLICK);
        timeout = AI_TIMEOUT;
        return;
    }

    // Setup for move computing
    int moveX = playX;
    int moveY = playY;
    rangeIndicator->showMove();
    rangeIndicator->showNone();

    // Get terrain
    std::vector<bool> onGrass;
    for(unsigned int i = 0; i < enemy.size(); i++)
        onGrass.push_back(map->getTerrain(enemy[i]->getX(),enemy[i]->getY())==GRASS+PLAYER);

    // Forward!
    if(player->canShoot()){

        // Get target
        Player* target = NULL;
        int bestDist = 99999999;
        for(unsigned int i = 0; i < enemy.size(); i++){
            Player* current = enemy[i];
            int dist = getDist(playX, playY, current);
            if(dist > 0 && dist < bestDist){
                target = current;
                bestDist = dist;
            }
        }

        // Init
        int bestDamage = 0;
        int bestCover = 0;
        bool clearShoot = false;

        // Check all moves
        for(int i = -player->getSpeed(); i <= player->getSpeed(); i++){
            for(int j = -player->getSpeed(); j <= player->getSpeed(); j++){

                // Invalid move: go next
                if(!(rangeIndicator->validMove(playX+i, playY+j)))
                    continue;

                // If clear shot
                int range = getRange(playX+i, playY+j, target);
                bool playerOnGrass = (map->getTerrain(playX+i, playY+j)==GRASS+PLAYER);
                if(range > 0){
                    int dmg = getBestWeapon(range, player, playerOnGrass);
                    if(dmg > bestDamage){
                        clearShoot = true;
                        bestDamage = dmg;
                        moveX = playX+i;
                        moveY = playY+j;
                    }
                    continue;
                }

                // If already have clear shot but this aint it
                if(clearShoot)
                    continue;

                // Get ranges, dist and if clear
                int dist = getDist(playX+i, playY+j, target);
                int cover = 0;
                // If out of range: boost priority
                for(unsigned int i = 0; i < enemy.size(); i++){
                    Player* current = enemy[i];
                    int range = getRange(playX+i, playY+j, current);
                    if(range <= 0 || getBestWeapon(range, current, onGrass[i]) == 0)
                        cover++;
                }

                // If all clear and better than before: go there!
                if((cover==bestCover && dist<bestDist) || (cover > bestCover)){
                    bestDist = dist;
                    bestCover = cover;
                    moveX = playX+i;
                    moveY = playY+j;
                }

            }
        }

    }

    // Find cover!
    else{

        // Init stuff
        int bestScore = 0;
        bool foundBest = false;

        // Check all moves
        for(int i = -player->getSpeed(); i <= player->getSpeed(); i++){
            for(int j = -player->getSpeed(); j <= player->getSpeed(); j++){

                // Invalid move: go next
                if(!(rangeIndicator->validMove(playX+i, playY+j)))
                    continue;

                // Get ranges + terrain info
                std::vector<int> range;
                for(unsigned int i = 0; i < enemy.size(); i++)
                    range.push_back(getRange(playX+i, playY+j, enemy[i]));

                // If hidden from all: break early
                if(range[0] <= 0 && range[1] <= 0 && range[2] <= 0){
                    moveX = playX + i;
                    moveY = playY + j;
                    foundBest = true;
                    break;
                }

                // If out of range: boost priority
                for(unsigned int i = 0; i < enemy.size(); i++){
                    if(range[i] <= 0 || getBestWeapon(range[i], enemy[i], onGrass[i]) == 0)
                        range[i] = L_NUM;
                }

                // If new best pos
                int sum = range[0] + range[1] + range[2];
                if(sum > bestScore){
                    moveX = playX + i;
                    moveY = playY + j;
                    bestScore = sum;
                }

            }

            // Why is there no double break ...
            if(foundBest)
                break;
        }
    }

    // Do move
    if(!(moveX == playX && moveY == playY)){
        ui->processClick(TOOLBAR_X, TOOLBAR_MOVE, LCLICK);
        ui->processClick(moveX*BLOCK + S_NUM, moveY*BLOCK + S_NUM, LCLICK);
    }

    // Always set moved even if didnt actually do it
    player->setMoved();
    timeout = AI_TIMEOUT;

}

// Calculate range between point and target
int AI::getRange(int x, int y, Player* target){

    // Check target alive
    if(target->isDead())
        return 0;

    // Calc gradient
    float diffX = target->getX() - x;
    float diffY = target->getY() - y;
    if(diffX == 0) diffX = 0.001f;
    float grad = diffY / diffX;

    // Dist variables
    float currentX = (x*BLOCK) + BLOCK/2;
    float currentY = (y*BLOCK) + BLOCK/2;
    int gridX = x;
    int gridY = y;
    int targetX = target->getX();
    int targetY = target->getY();

    // Check no rocks / players in the way
    while(true){

        // Update dist
        if(std::abs(grad) > 1){
            if(target->getY() > y){
                currentY++;
                currentX+=1/grad;
            }
            else{
                currentY--;
                currentX-=1/grad;
            }
        }
        else{
            if(target->getX() > x){
                currentX++;
                currentY+=grad;
            }
            else{
                currentX--;
                currentY-=grad;
            }
        }
        gridX = currentX / BLOCK;
        gridY = currentY / BLOCK;

        // If at dest
        if(gridX == targetX && gridY == targetY)
            break;

        // If start grid: skip
        if(gridX == x && gridY == y)
            continue;

        // Outside of area
        if(gridX < 0 || gridY < 0 || gridX >= TILE_X || gridY >= TILE_Y)
            return -1;

        // If current terrain would block: return not possible
        if(map->getTerrain(gridX, gridY) >= ROCK)
            return -1;

    }

    // Calc distance
    int xDist = std::pow(target->getX()*BLOCK - (x*BLOCK),2);
    int yDist = std::pow(target->getY()*BLOCK - (y*BLOCK),2);
    return std::sqrt(xDist + yDist);

}

// Get distance
int AI::getDist(int x, int y, Player* target){

    // If dead: return 0
    if(target->isDead())
        return 0;

    // Else Pythag
    int xDist = std::pow(target->getX()*BLOCK - (x*BLOCK),2);
    int yDist = std::pow(target->getY()*BLOCK - (y*BLOCK),2);
    return std::sqrt(xDist + yDist);
}

// Get the best weapon for the job!
int AI::getBestWeapon(int dist, Player* player, bool onGrass){

    // If dist <= 0: dead / no line of sight
    if(dist <= 0)
        return 0;

    // Get range and stuff
    Weapon* pri = player->getPrimary();
    Weapon* sec = player->getSecondary();
    int priAcc = pri->getAccuracy();
    int secAcc = sec->getAccuracy();
    if(onGrass){
        priAcc /= 2;
        secAcc /= 2;
    }
    int priMaxRange = (BASE_MAX_RANGE+SCALING_MAX_RANGE*priAcc)*BLOCK;
    int secMaxRange = (BASE_MAX_RANGE+SCALING_MAX_RANGE*secAcc)*BLOCK;

    // If sec in damage
    if(secMaxRange >= dist && (sec->getDamage() > pri->getDamage()))
        return sec->getDamage();
    if(priMaxRange >= dist)
        return pri->getDamage();
    return 0;

}
