#include"ui.hpp"

// Constructor
UI::UI(Map* map, RangeIndicator* rangeIndicator, Shot* shot, Toolbar* toolbar, std::vector<std::string> loadouts){
    this->map = map;
    this->rangeIndicator = rangeIndicator;
    this->shot = shot;
    this->toolbar = toolbar;
    this->loadouts = loadouts;
    mode = MAIN;
    redAI = AI_RED_DEFAULT;
    blueAI = AI_BLUE_DEFAULT;
    isAIPause = false;
}

// Add players
void UI::addPlayers(std::vector<Player*> players){
  this->players = players;
}

// Set side
void UI::setSide(int side) { this->side = side; }
void UI::setActivePlayer(Player* player){
    activePlayer = player;
    toolbar->setPlayer(player);
    rangeIndicator->setPlayer(player);
}

// Getters
int UI::getMode(){ return mode; }
bool UI::isAI(int side){
    if(side == RED && redAI)
        return true;
    if(side == BLUE && blueAI)
        return true;
    return false;
}

// Return to select if in mode or pause
void UI::escKey(){
    if(mode > SELECT){
        mode = SELECT;
        rangeIndicator->showNone();
    }
    else if(mode == PAUSE){
        if(isAIPause){
            isAIPause = false;
            mode = prevMode;
        }
        else
            mode = SELECT;
    }
    else if(mode == SELECT)
        mode = PAUSE;
}

void UI::AIPause(){
    prevMode = mode;
    mode = PAUSE;
    isAIPause = true;
}

void UI::setVict(){ mode = VICTORY; }

// Process a click
bool UI::processClick(int x, int y, sf::Mouse::Button button){
    if(mode == MAIN)
        return processMain(x,y,button);
    else if(mode == ABOUT1)
        processAbout1(x,y,button);
    else if(mode == ABOUT2)
        processAbout2(x,y,button);
    else if(mode == LOBBY)
        return processLobby(x,y,button);
    else if(mode == CHAMP_SELECT)
        return processChampSelect(x,y,button);
    else if(mode == PAUSE)
        processPause(x,y,button);
    else if(mode == VICTORY)
        processVict(x,y,button);
    else
        processGame(x,y,button);
    return false;
}

// Process click on main
bool UI::processMain(int x, int y, sf::Mouse::Button button){

    // Weed out wrong button or invalid x range
    if(button == RCLICK || x < 830 || x > 1100)
        return false;

    // New game - go to lobby
    if(y > 480 && y < 540){
        mode = LOBBY;
        players[0]->loadFromFile(PLAYER11_LOADOUT);
        players[1]->loadFromFile(PLAYER12_LOADOUT);
        players[2]->loadFromFile(PLAYER13_LOADOUT);
        players[3]->loadFromFile(PLAYER21_LOADOUT);
        players[4]->loadFromFile(PLAYER22_LOADOUT);
        players[5]->loadFromFile(PLAYER23_LOADOUT);
        redAI = AI_RED_DEFAULT;
        blueAI = AI_BLUE_DEFAULT;
        return true;
    }
    // About
    else if(y > 560 && y < 620)
        mode = ABOUT1;
    // Quit
    else if(y > 650 && y < 710)
        mode = QUIT;

    return false;

}

// Process click on about page 1
void UI::processAbout1(int x, int y, sf::Mouse::Button button){
    if(button == LCLICK)
        mode = ABOUT2;
}

// Process click on about page 2
void UI::processAbout2(int x, int y, sf::Mouse::Button button){
    if(button == LCLICK)
        mode = MAIN;
    else if(button == RCLICK)
        mode = ABOUT1;
}

// Process click in lobby
bool UI::processLobby(int x, int y, sf::Mouse::Button button){

    // Wtf who right clicks?
    if(button == RCLICK)
        return false;

    // Start game
    if(x > 840 && x < 1070 && y > 840 && y < 900){

        // Reset stuff
        rangeIndicator->showNone();
        map->reroll();
        shot->setInactive();

        // Place
        players[0]->setPosition(SPAWN_X_RED, SPAWN_Y_1);
        players[1]->setPosition(SPAWN_X_RED, SPAWN_Y_2);
        players[2]->setPosition(SPAWN_X_RED, SPAWN_Y_3);
        players[3]->setPosition(SPAWN_X_BLUE, SPAWN_Y_1);
        players[4]->setPosition(SPAWN_X_BLUE, SPAWN_Y_2);
        players[5]->setPosition(SPAWN_X_BLUE, SPAWN_Y_3);

        // Generate character if chose rand
        srand(time(NULL));
        for(unsigned int i = 0; i < players.size(); i++){
            Player* current = players[i];
            if(current->getName().compare("?") == 0)
                current->loadFromFile(loadouts[rand()%(loadouts.size() - 1)]);
        }

        // Set red to start
        for(unsigned int i = BLUE_START; i < BLUE_END; i++){
            Player* current = players[i];
            current->setShot();
            current->setMoved();
        }

        // Flow stuff
        mode = SELECT;
        return false;
    }

    // Cancel
    if(x > 910 && x < 1000 && y > 940 && y < 980){
        mode = MAIN;
        return false;
    }

    // Toggle redside AI
    if(x > 830 && x < 1080 && y > 435 && y < 485){
        redAI = !redAI;
        return true;
    }
    // Toggle blueside AI
    if(x > 830 && x < 1080 && y > 600 && y < 650){
        blueAI = !blueAI;
        return true;
    }

    // Select certain player
    if(y > 325 && y < 400){
        if(x > 610 && x < 810){
            activePlayer = players[0];
            mode = CHAMP_SELECT;
        }
        else if(x > 860 && x < 1060){
            activePlayer = players[1];
            mode = CHAMP_SELECT;
        }
        else if(x > 1110 && x < 1310){
            activePlayer = players[2];
            mode = CHAMP_SELECT;
        }
    }
    else if(y > 685 && y < 760){
        if(x > 610 && x < 810){
            activePlayer = players[3];
            mode = CHAMP_SELECT;
        }
        else if(x > 860 && x < 1060){
            activePlayer = players[4];
            mode = CHAMP_SELECT;
        }
        else if(x > 1110 && x < 1310){
            activePlayer = players[5];
            mode = CHAMP_SELECT;
        }
    }

    return false;
}

// Process click in champ select
bool UI::processChampSelect(int x, int y, sf::Mouse::Button button){

    // Really ... right click does nothing
    if(button == RCLICK)
        return false;

    // Always set back to lobby
    mode = LOBBY;

    // Calc size
    int csRows = loadouts.size()/3;
    if(loadouts.size()%3 != 0) csRows++;
    int baseCS_X = 960 - (3*CS_COL_SIZE/2);
    int baseCS_Y = 540 - (csRows*CS_ROW_SIZE/2);
    int capCS_X = baseCS_X + 3*CS_COL_SIZE;
    int capCS_Y = baseCS_Y + csRows*CS_ROW_SIZE;

    // If within champ select box
    if(x > baseCS_X && x < capCS_X && y > baseCS_Y && y < capCS_Y){
        int row = (y-baseCS_Y)/CS_ROW_SIZE;
        int col = (x-baseCS_X)/CS_COL_SIZE;
        activePlayer->loadFromFile(loadouts[row*3 + col]);
        return true;
    }

    return false;
}

// Process click during game pause
void UI::processPause(int x, int y, sf::Mouse::Button button){
    if(button == RCLICK) return;
    if(x > 720 && y > 430 && x < 1200 && y < 650)
        mode = MAIN;
    else{
        if(isAIPause){
            mode = prevMode;
            isAIPause = false;
        }
        else
            mode = SELECT;
    }
}

// Process victory screen
void UI::processVict(int x, int y, sf::Mouse::Button button){
    if(button == sf::Mouse::Button::Left)
        mode = MAIN;
}

// Process click in game
void UI::processGame(int x, int y, sf::Mouse::Button button){

    // Nothing happens while a shot is going on
    if(shot->isActive())
        return;

    // Right click = cancel
    if(button == RCLICK){
        mode = SELECT;
        rangeIndicator->showNone();
        return;
    }

    // Toolbar
    if(x > 1722 && x < 1920){

        // Wrong team: only skip works
        if(activePlayer->getSide() != side){
            if(y > 1030 && y < 1070)
                selectNextPlayer();
            return;
        }

        // Move
        if(y > 10 && y < 240){
            if(mode == MOVE){
                rangeIndicator->showNone();
                mode = SELECT;
            }
            else if(activePlayer->canMove()){
                rangeIndicator->showMove();
                mode = MOVE;
            }
        }
        // Primary
        else if(y > 250 && y < 630){
            if(mode == SHOT_PRI){
                rangeIndicator->showNone();
                mode = SELECT;
            }
            else if(activePlayer->canShoot()){
                rangeIndicator->showPrimary();
                mode = SHOT_PRI;
            }
        }
        // Secondary
        else if(y > 640 && y < 1020){
            if(mode == SHOT_SEC){
                rangeIndicator->showNone();
                mode = SELECT;
            }
            else if(activePlayer->canShoot()){
                rangeIndicator->showSecondary();
                mode = SHOT_SEC;
            }
        }
        // Skip
        else if(y > 1030 && y < 1070){
            rangeIndicator->showNone();
            activePlayer->setShot();
            activePlayer->setMoved();
            selectNextPlayer();
            mode = SELECT;
       }

        return;
    }

    // Get coords
    int coordX = x / BLOCK;
    int coordY = y / BLOCK;

    // Select mode
    if(mode == SELECT){
        Player* temp = map->getPlayer(coordX, coordY);
        if(temp != NULL)
            setActivePlayer(temp);
    }
    // Move mode
    else if(mode == MOVE && activePlayer->canMove()){
        if(rangeIndicator->validMove(coordX, coordY)){
            activePlayer->setPosition(coordX, coordY);
            mode = SELECT;
            rangeIndicator->showNone();
            toolbar->refresh();
            activePlayer->setMoved();
            if(activePlayer->turnOver())
                selectNextPlayer();
        }
    }
    // Shoot primary
    else if(mode == SHOT_PRI && activePlayer->canShoot()){
        shot->startShot(activePlayer->getX(), activePlayer->getY(),
            activePlayer->getPrimary(), x, y);
        mode = SELECT;
        activePlayer->setShot();
        if(activePlayer->turnOver())
            selectNextPlayer();
    }
    // Shoot secondary
    else if(mode == SHOT_SEC && activePlayer->canShoot()){
        shot->startShot(activePlayer->getX(), activePlayer->getY(),
            activePlayer->getSecondary(), x, y);
        mode = SELECT;
        activePlayer->setShot();
        if(activePlayer->turnOver())
            selectNextPlayer();
    }

    return;

}

// Select next player that can still do something if turn over
void UI::selectNextPlayer(){

    if(!shot->isActive())
        rangeIndicator->showNone();
    mode = SELECT;

    // Red side
    if(side == RED){

        // If active blue - set to last red and get next
        if(activePlayer->getSide() != RED)
            activePlayer = players[2];

        // Get next in order
        for(int i = RED_START; i < RED_END; i++){
            Player* current = players[i];
            if(activePlayer == current){
                if(players[(i+1)%3]->canDoSomething()){
                    setActivePlayer(players[(i+1)%3]);
                    break;
                }
                else if(players[(i+2)%3]->canDoSomething()){
                    setActivePlayer(players[(i+2)%3]);
                    break;
                }
            }

        }
    }

    // Blue side
    else if(side == BLUE){

        // If active red - set to last blue and get next
        if(activePlayer->getSide() != BLUE)
            activePlayer = players[5];

        // Get next in order
        for(int i = BLUE_START; i < BLUE_END; i++){
            Player* current = players[i];
            if(activePlayer == current){
                if(players[((i+1)%3)+3]->canDoSomething()){
                    setActivePlayer(players[((i+1)%3)+3]);
                    break;
                }
                else if(players[((i+2)%3)+3]->canDoSomething()){
                    setActivePlayer(players[((i+2)%3)+3]);
                    break;
                }
            }
        }
    }
}

// Make sure didnt select somebody who died during the shot
void UI::checkStillAlive(){
    if(activePlayer->isDead())
        selectNextPlayer();
}
