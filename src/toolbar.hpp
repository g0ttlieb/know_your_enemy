#ifndef TOOLBAR_HPP
#define TOOLBAR_HPP

#include<string>

#include"player.hpp"
#include"define.hpp"

class Toolbar : public sf::Drawable, public sf::Transformable {
    
public:

    // Constructor
    Toolbar();

    // Change what showing
    void setPlayer(Player* player);
    void setSide(int side);
    void refresh();

private:
    
    // Current player
    Player* player;

    // Player outline
    sf::Texture spotlight_texture;
    sf::Sprite spotlight_sprite;

    // Text boxes
    sf::Font font;
    sf::Color colour;
    sf::Text name;
    sf::Text hp;
    sf::Text speed;
    sf::Text priDmg;
    sf::Text priAcc;
    sf::Text priBlt;
    sf::Text priTyp;
    sf::Text secDmg;
    sf::Text secAcc;
    sf::Text secBlt;
    sf::Text secTyp;

    // Tints
    sf::Texture redTintTexture;
    sf::Texture blueTintTexture;
    sf::Sprite moveTint;
    sf::Sprite shootTint;
    sf::Sprite skipTint;

    // Draw base texture
    sf::Texture toolbar_texture;
    sf::Sprite toolbar_sprite;

    // Helper for setting strings
    void setTextString(sf::Text* text, std::string string);

    // Draw the toolbar
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif
