#include"weapon.hpp"

// Constructors
Weapon::Weapon(){}
Weapon::Weapon(int dmg, int acc, bool blast, bool singleShot){
    this->dmg = dmg;
    this->acc = acc;
    this->blast = blast;
    this->singleShot = singleShot;
}

// Getters
int Weapon::getDamage(){ return dmg; }
int Weapon::getAccuracy(){ return acc; }
bool Weapon::isBlast(){ return blast; }
bool Weapon::isSingleShot(){ return singleShot; }
