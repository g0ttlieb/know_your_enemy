#include"toolbar.hpp"

// Constructor
Toolbar::Toolbar(){

    // Toolbar sprite
    toolbar_texture.loadFromFile("textures/toolbar.png");
    toolbar_sprite.setTexture(toolbar_texture);
    toolbar_sprite.setPosition(1728,0);

    // Spotlight sprite
    spotlight_texture.loadFromFile("textures/spotlight.png");
    spotlight_sprite.setTexture(spotlight_texture);

    // Load font and colour
    font.loadFromFile("fonts/pixel.ttf");
    colour = sf::Color(33,38,53);

    // Setup all font instances
    name.setFont(font);
    name.setCharacterSize(WORD_FONT_SIZE);
    name.setPosition(1824, 35);
    name.setFillColor(colour);
    hp.setFont(font);
    hp.setCharacterSize(NUM_FONT_SIZE);
    hp.setPosition(TOOLBAR_TXT_X_POS,116);
    hp.setFillColor(colour);
    speed.setFont(font);
    speed.setCharacterSize(NUM_FONT_SIZE);
    speed.setPosition(TOOLBAR_TXT_X_POS,188);
    speed.setFillColor(colour);
    priDmg.setFont(font);
    priDmg.setCharacterSize(NUM_FONT_SIZE);
    priDmg.setPosition(TOOLBAR_TXT_X_POS,364);
    priDmg.setFillColor(colour);
    priAcc.setFont(font);
    priAcc.setCharacterSize(NUM_FONT_SIZE);
    priAcc.setPosition(TOOLBAR_TXT_X_POS,436);
    priAcc.setFillColor(colour);
    priBlt.setFont(font);
    priBlt.setCharacterSize(BOOL_FONT_SIZE);
    priBlt.setPosition(TOOLBAR_TXT_X_POS,508);
    priBlt.setFillColor(colour);
    priTyp.setFont(font);
    priTyp.setCharacterSize(BOOL_FONT_SIZE);
    priTyp.setPosition(TOOLBAR_TXT_X_POS,580);
    priTyp.setFillColor(colour);
    secDmg.setFont(font);
    secDmg.setCharacterSize(NUM_FONT_SIZE);
    secDmg.setPosition(TOOLBAR_TXT_X_POS,756);
    secDmg.setFillColor(colour);
    secAcc.setFont(font);
    secAcc.setCharacterSize(NUM_FONT_SIZE);
    secAcc.setPosition(TOOLBAR_TXT_X_POS,828);
    secAcc.setFillColor(colour);
    secBlt.setFont(font);
    secBlt.setCharacterSize(BOOL_FONT_SIZE);
    secBlt.setPosition(TOOLBAR_TXT_X_POS,900);
    secBlt.setFillColor(colour);
    secTyp.setFont(font);
    secTyp.setCharacterSize(BOOL_FONT_SIZE);
    secTyp.setPosition(TOOLBAR_TXT_X_POS,972);
    secTyp.setFillColor(colour);

    // Setup tint
    redTintTexture.loadFromFile("textures/redTint.png");
    redTintTexture.setRepeated(true);
    blueTintTexture.loadFromFile("textures/blueTint.png");
    blueTintTexture.setRepeated(true);
    moveTint.setTextureRect({0,0,192,243});
    moveTint.setPosition(1728, 0);
    shootTint.setTextureRect({0,0,192,783});
    shootTint.setPosition(1728,244);
    skipTint.setTextureRect({0,0,192,52});
    skipTint.setPosition(1728,1028);

}

// Set new player and refresh
void Toolbar::setPlayer(Player* player){
    this->player = player;
    refresh();
    if(player->getSide() == BLUE){
        moveTint.setTexture(blueTintTexture);
        shootTint.setTexture(blueTintTexture);
    }
    else{
        moveTint.setTexture(redTintTexture);
        shootTint.setTexture(redTintTexture);
    }
}

void Toolbar::setSide(int side){
    if(side == BLUE)
        skipTint.setTexture(blueTintTexture);
    else
        skipTint.setTexture(redTintTexture);
}

// Refresh info
void Toolbar::refresh(){

    // Update spotlight pos
    spotlight_sprite.setPosition(player->getX()*BLOCK-3, player->getY()*BLOCK-3);

    // Update player info
    setTextString(&name, player->getName());
    setTextString(&hp, std::to_string(player->getHealth()));
    setTextString(&speed, std::to_string(player->getSpeed()));

    Weapon* weapon;

    // Update primary weapon
    weapon = player->getPrimary();
    setTextString(&priDmg, std::to_string(weapon->getDamage()));
    setTextString(&priAcc, std::to_string(weapon->getAccuracy()));
    if(weapon->isBlast())
        setTextString(&priBlt, "Yes");
    else
        setTextString(&priBlt, "No");
    if(weapon->isSingleShot())
        setTextString(&priTyp, "No");
    else
        setTextString(&priTyp, "Yes");

    // Update secondary weapon
    weapon = player->getSecondary();
    setTextString(&secDmg, std::to_string(weapon->getDamage()));
    setTextString(&secAcc, std::to_string(weapon->getAccuracy()));
    if(weapon->isBlast())
        setTextString(&secBlt, "Yes");
    else
        setTextString(&secBlt, "No");
    if(weapon->isSingleShot())
        setTextString(&secTyp, "No");
    else
        setTextString(&secTyp, "Yes");

}

// Set string and center text field
void Toolbar::setTextString(sf::Text* text, std::string string){
    text->setString(string);
    sf::FloatRect bounds = text->getLocalBounds();
    text->setOrigin(bounds.left+bounds.width/2.0f, bounds.top+bounds.height/2.0f);
}

// Draw the sprite
void Toolbar::draw(sf::RenderTarget& target, sf::RenderStates states) const{

    // Draw toolbar
    states.transform *= getTransform();
    states.texture = &toolbar_texture;
    target.draw(toolbar_sprite, states);

    // Draw spotlight
    target.draw(spotlight_sprite);

    // Draw fonts
    target.draw(name);
    target.draw(hp);
    target.draw(speed);
    target.draw(priDmg);
    target.draw(priAcc);
    target.draw(priBlt);
    target.draw(priTyp);
    target.draw(secDmg);
    target.draw(secAcc);
    target.draw(secBlt);
    target.draw(secTyp);

    // Draw tints
    if(player->canMove())
        target.draw(moveTint);
    if(player->canShoot())
        target.draw(shootTint);
    target.draw(skipTint);

}
