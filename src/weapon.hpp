#ifndef WEAPON_HPP
#define WEAPON_HPP

class Weapon{
    
public:

    // Constructors
    Weapon();
    Weapon(int dmg, int acc, bool blast, bool singleShot);

    // Getters
    int getDamage();
    int getAccuracy();
    bool isBlast();
    bool isSingleShot();

private:

    // Class variables
    int dmg;
    int acc;
    bool blast;
    bool singleShot;

};

#endif
