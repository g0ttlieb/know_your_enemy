#ifndef MAP_HPP
#define MAP_HPP

#include <vector>
#include"player.hpp"
#include"define.hpp"

class Map : public sf::Drawable, public sf::Transformable {

public:

    // Constructor
    Map();
    void reroll();
    void addPlayers(std::vector<Player*> players);
    int getTerrain(int x, int y);
    Player* getPlayer(int x, int y);

private:

    // Class variables
    sf::Texture map_texture;
    sf::VertexArray map_vertices;
    int rocks;
    int level[TILE_Y][TILE_X];

    // Store players
    std::vector<Player*> players;

    // Generate level setup
    int generateLevel();
    void loadSprite();

    // Draw the map
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif
