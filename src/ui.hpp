#ifndef UI_HPP
#define UI_HPP

#include <vector>
#include"define.hpp"
#include"map.hpp"
#include"player.hpp"
#include"range_indicator.hpp"
#include"shot.hpp"
#include"toolbar.hpp"

class UI{

public:

    // Constructors
    UI(Map* map, RangeIndicator* rangeIndicator, Shot* shot, Toolbar* toolbar, std::vector<std::string> loadouts);
    void addPlayers(std::vector<Player*> players);

    // Setters
    void setSide(int side);
    void setActivePlayer(Player* player);
    void selectNextPlayer();
    void checkStillAlive();

    // Getter
    int getMode();
    bool isAI(int side);

    // Process
    void escKey();
    void AIPause();
    void setVict();
    bool processClick(int x, int y, sf::Mouse::Button button);

private:

    // Class variables
    int side;
    int mode;
    bool redAI;
    bool blueAI;
    Map* map;
    RangeIndicator* rangeIndicator;
    Shot* shot;
    Toolbar* toolbar;
    std::vector<Player*> players;
    Player* activePlayer;
    std::vector<std::string> loadouts;

    // AI Pause stuff
    int prevMode;
    bool isAIPause;

    // Process depending on mode
    bool processMain(int x, int y, sf::Mouse::Button button);
    void processAbout1(int x, int y, sf::Mouse::Button button);
    void processAbout2(int x, int y, sf::Mouse::Button button);
    bool processLobby(int x, int y, sf::Mouse::Button button);
    bool processChampSelect(int x, int y, sf::Mouse::Button button);
    void processPause(int x, int y, sf::Mouse::Button button);
    void processVict(int x, int y, sf::Mouse::Button button);
    void processGame(int x, int y, sf::Mouse::Button button);

};

#endif
