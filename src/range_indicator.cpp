#include"range_indicator.hpp"

#include<iostream>
#include<cmath>

// Constructor
RangeIndicator::RangeIndicator(Map* map){
    player=NULL;
    this->map = map;
    ri_texture.loadFromFile("textures/rangeIndicator.png");
    ri_vertices.setPrimitiveType(sf::Quads);
    ri_vertices.clear();
    showArc = false;
    rangeArc.setTexture(&ri_texture);
    firstTime = true;
}

// Change active player
void RangeIndicator::setPlayer(Player* player){
    this->player = player;
}

// Show move range of player
void RangeIndicator::showMove(){

    showArc = false;

    // Initial vals needed
    int range = player->getSpeed();
    int playerX = player->getX();
    int playerY = player->getY();

    // Resize
    ri_vertices.clear();
    ri_vertices.resize((2*range+1)*(2*range+1)*4);

    // Delete old if first time
    if(!firstTime)
        freeMem();
    else
        firstTime = false;

    // Setup smaller array to calculate
    baseX = 0;
    baseY = 0;
    capX = TILE_X;
    capY = TILE_Y;
    if(playerX - range > 0) baseX = playerX - range;
    if(playerY - range > 0) baseY = playerY - range;
    if(playerX + range < capX) capX = playerX + range + 1;
    if(playerY + range < capY) capY = playerY + range + 1;
    
    // Init to cant reach (-1)
    dist = new int*[capY - baseY];
    for(int i = 0; i < capY - baseY; i++){
        dist[i] = new int[capX - baseX];
        for(int j = 0; j < capX - baseX; j++)
            dist[i][j] = -1;
    }

    // Set origin and recurse
    dist[playerY - baseY][playerX - baseX] = range;
    recurseDist(playerX - baseX, playerY - baseY);
    dist[playerY - baseY][playerX - baseX] = -1;

    // Add points as required
    int pos = 0;
    for(int i = 0; i < capY - baseY; i++){
        for(int j = 0; j < capX - baseX; j++){
            if(dist[i][j] >= 0)
                addPoint(pos++, baseX + j, baseY + i);
        }
    }

}

// Show range of primary weapon
void RangeIndicator::showPrimary(){
    showArc = true;
    createArc(player->getPrimary());
}

// Show range of secondary weapon
void RangeIndicator::showSecondary(){
    showArc = true;
    createArc(player->getSecondary());
}

void RangeIndicator::showNone(){
    showArc = false;
    ri_vertices.clear();
}

// Check if pos is valid place to move
bool RangeIndicator::validMove(int x, int y){
    if(x < baseX || x >= capX || y < baseY || y >= capY)
        return false;
    return (dist[y - baseY][x - baseX] != -1);
}

// If arc: change to rotation of mouse
void RangeIndicator::refresh(int x, int y){

    // Filter
    if(!showArc) return;

    // Get diffs
    int xDiff = x - player->getX()*BLOCK - 0.5f*BLOCK;
    int yDiff = y - player->getY()*BLOCK - 0.5f*BLOCK;
    if(xDiff == 0) xDiff = 0.0001f;                     // Prevent / 0

    // Calc angle and set
    float angle = atan(((float) yDiff)/((float) xDiff))*180.0f/(4.0f*atan(1.0f));
    if(x >= player->getX()*BLOCK + 0.5f*BLOCK)
        angle+=90;
    else
        angle+=270;

    rangeArc.setRotation(angle);
}

// Delete at end
void RangeIndicator::freeMem(){
    for(int i = 0; i < capY - baseY; i++)
        delete[] dist[i];
    delete[] dist;
}

// Add point to vertex array
void RangeIndicator::addPoint(int index, int x, int y){

    // Set pos
    ri_vertices[4*index].position = sf::Vector2f(x*BLOCK, y*BLOCK);
    ri_vertices[4*index+1].position = sf::Vector2f((x+1)*BLOCK, y*BLOCK);
    ri_vertices[4*index+2].position = sf::Vector2f((x+1)*BLOCK, (y+1)*BLOCK);
    ri_vertices[4*index+3].position = sf::Vector2f(x*BLOCK, (y+1)*BLOCK);

    // Set texture coords
    ri_vertices[4*index].texCoords = sf::Vector2f(0,0);
    ri_vertices[4*index+1].texCoords = sf::Vector2f(BLOCK,0);
    ri_vertices[4*index+2].texCoords = sf::Vector2f(BLOCK, BLOCK);
    ri_vertices[4*index+3].texCoords = sf::Vector2f(0,BLOCK);

}

// Recursively solve distance
void RangeIndicator::recurseDist(int x, int y){

    
    // Base case
    if(dist[y][x] == -1)
        return;
        
    // Get val that will spread
    int curTerrain = map->getTerrain(baseX + x, baseY + y);
    int moveTerrain;
    int val = dist[y][x] - 1;
    
    // Left
    if(x == 0)
        moveTerrain = ROCK;
    else
        moveTerrain = map->getTerrain(baseX + x - 1, baseY + y);
    if(moveTerrain < ROCK){
        if((moveTerrain == RIVER || curTerrain == RIVER) && dist[y][x-1] < val-1){
            dist[y][x-1] = val-1;
            recurseDist(x-1, y);
        }
        else if(dist[y][x-1] < val && curTerrain!=RIVER && moveTerrain!=RIVER){
            dist[y][x-1] = val;
            recurseDist(x-1, y);
        }
    }
    
    // Right
    if(x == capX - baseX - 1)
        moveTerrain = ROCK;
    else
        moveTerrain = map->getTerrain(baseX + x + 1, baseY + y);
    if(moveTerrain < ROCK){
        if((moveTerrain == RIVER || curTerrain == RIVER) && dist[y][x+1] < val-1){
            dist[y][x+1] = val-1;
            recurseDist(x+1, y);
        }
        else if(dist[y][x+1] < val && curTerrain!=RIVER && moveTerrain!=RIVER){
            dist[y][x+1] = val;
            recurseDist(x+1, y);
        }
    }
    
    // Up
    if(y == 0)
        moveTerrain = ROCK;
    else
        moveTerrain = map->getTerrain(baseX + x, baseY + y - 1);
    if(moveTerrain < ROCK){
        if((moveTerrain == RIVER || curTerrain == RIVER) && dist[y-1][x] < val-1){
            dist[y-1][x] = val-1;
            recurseDist(x, y-1);
        }
        else if(dist[y-1][x] < val && curTerrain!=RIVER && moveTerrain!=RIVER){
            dist[y-1][x] = val;
            recurseDist(x, y-1);
        }
    }

    // Down
    if(y == capY - baseY - 1)
        moveTerrain = ROCK;
    else
        moveTerrain = map->getTerrain(baseX + x, baseY + y + 1);
    if(moveTerrain < ROCK){
        if((moveTerrain == RIVER || curTerrain == RIVER) && dist[y+1][x] < val-1){
            dist[y+1][x] = val-1;
            recurseDist(x, y+1);
        }
        else if(dist[y+1][x] < val && curTerrain!=RIVER && moveTerrain!=RIVER){
            dist[y+1][x] = val;
            recurseDist(x, y+1);
        }
    }

}

// Show arc for given weapon
void RangeIndicator::createArc(Weapon* weapon){

    // Calc accuracy
    int acc = weapon->getAccuracy();
    if(map->getTerrain(player->getX(), player->getY()) == GRASS + PLAYER)
        acc /= 2;
    float accRange=(BASE_ACC_RANGE+SCALING_ACC_RANGE*acc)*BLOCK;
    float maxRange=(BASE_MAX_RANGE+SCALING_MAX_RANGE*acc)*BLOCK;
    float angle = std::atan((float) BLOCK / (2.0f*accRange));

    // Calc points
    float rad_90_d = 2*atan(1);
    float xPos = maxRange * cos(rad_90_d - angle);
    float yPos = maxRange * sin(rad_90_d -angle);

    // Make arc - super basic!
    rangeArc.setPointCount(4);
    rangeArc.setPoint(0, sf::Vector2f(0,0));
    rangeArc.setPoint(1, sf::Vector2f(-xPos, -yPos));
    rangeArc.setPoint(2, sf::Vector2f(0, -maxRange));
    rangeArc.setPoint(3, sf::Vector2f(xPos, -yPos));
    
    // Change origin to middle bot
    sf::FloatRect bounds = rangeArc.getLocalBounds();
    rangeArc.setOrigin(bounds.left+bounds.width/2.0f, bounds.top + bounds.height);
    rangeArc.setPosition((player->getX()+0.5f)*BLOCK,(player->getY()+0.5f)*BLOCK);

}

// Draw the sprite
void RangeIndicator::draw(sf::RenderTarget& target, sf::RenderStates states) const{

    // Draw based on with method
    if(showArc)
        target.draw(rangeArc);
    else{
        states.transform *= getTransform();
        states.texture = &ri_texture;
        target.draw(ri_vertices, states);
    }
        
}
