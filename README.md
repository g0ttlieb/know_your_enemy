# Know Your Enemy

A turn-based strategy game. Yup ... that's it ...

A demo can be found here: [youtube](https://www.youtube.com/watch?v=A8tIzIlvo5s&feature=youtu.be)

NOTE: This was developed in 2018 before I had any formal AI or C++ training
(which I now have), coming from mainly a strong C background.
There are some weird stylistic things! I have fixed up some but ... there is
still a bit of weird stuff there that would take a decent amount of work
to get back on track. I would rather spend this time on learning and developing
so, for now, this is on the back burner.

## About

### Victory Condition
The basic premise of the game is a 3v3 turn based strategy game. Eliminate all
of your opposition to win! You can
see all of your oppositions stats, though, so its less about trying to figure
out what they can do and more about how you can deal with it.
If you think about it long enough its basically a battle royale I guess?

### Basic Gameplay
Every turn, each of your remaining soldiers can use both a move action and an
attack action. These
can be done in either order as so desired but cannot be interleaved.
They can shoot either their primary or secondary weapon in their attack
action. The players turn ends once all solider have finished both of their actions
(be it used or skipped).

### The Map
The map greatly impacts the gameplay and decision making and so should be
considered wisely. There are 4 basic tiles:

1. The **ground** tile is overwhelmingly normal. Yep.

2. The **water** tile count as 2 for the purpose of movement. Be careful about
   being caught out!

3. The **rock** tile blocks shots (both coming and going) so is a great asset and
   liability.

4. The **grass** tile impacts accuracy. Shots taken while in grass have a 33%
   chance to miss and continue on but the accuracy stat of a player in grass is
   halved. Choose wisely when to duck in and out of it.

### Player Stats
There are 2 key stats for the given character:

1. **Health** is the number of damage that can be taken before elimination.

2. **Speed** is the number of tiles that can be traversed in a single turn.

Each player has 2 weapons with have their own unique stats:

1. **Damage** is the number of damage done by the weapon.

2. **Accuracy** impacts the range and hit chance of the weapon.

3. **Blast** is whether the weapon is single target or will
   explode to damage all surrounding tile. Damage is divided by 3 for
   collateral tiles (rounded down). It is denoted as either Yes (blast) or No (not blast)

4. **Spray** is the firing type of the weapon: either all damage is done
   in one projectile or each damage is done separately in multiple
   shots (with individual hit chance etc.). It is denoted by Yes (multi-shot) or
   No (single-shot).

Each character has a different selection of stats giving them certain
advantages and disadvantages. The primary is always longer range while
the secondary has a much shorter range, but generally higher damage.

### Advanced Shooting

The shooting mechanisms works by generating a field of fire based on the
accuracy of the weapon used. At 0 accuracy, a weapon will 100% hit a
target 1 square away and will have a max range of 3 squares distance.
These increase by 1 and 2 squares respectively, meaning
increasing accuracy narrows and lengthens the associated cone.

For each projectile in the attack move, the actual path taken is
randomly within the cone. If this impacts with a rock or
character it stops. If it is a character (be it friendly or opposing!), then
damage is also applied. Being
in grass gives a chance to avoid this damage and have the projectile continue
on its path. If a blast weapon impacts (be it due to max range or impact), damage
is applied to that and all surrounding tiles. Long grass wont save you from
this ...

### UI

The main mode is selection, allowing you to left click on any character to see
their stats. If an ally character is selected then you can carry out an
action. Clicking the top panel (character stats) will open up the move
action, highlighting available spaces to move which can be left clicked to move
there. The middle 2 panels will open up the primary and secondary
weapon attack actions respectively, displaying the field of fire. Left clicking
will fire that weapon. The final panel skips the remaining actions of the
selected character for that turn. At any point, a right click will cancel out
of the current action and return to the selection mode.

### Keyboard Shortcuts
1. **Esc**: Cancel, Pause / Unpause
2. **Space**: Skip turn
3. **Q**: Open / close move action
4. **W**: Open / close primary fire action
5. **E**: Open / close secondary fire action
6. **R**: Select next available player


## TODO

1. Clean up codebase - redesign main loop, break down into small functions, replace define.hpp with const static variables, make consts fraction of resolution not hardcoded 1080p
2. Improve champ select - see stats of the class
3. More sprites - different per class
